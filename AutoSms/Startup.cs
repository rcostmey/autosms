﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using AutoSms.Models;
using Hangfire;



namespace AutoSms
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
                     
        }

        public static IConfiguration Configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           
           services.AddDbContext<AutoSmsContext>(options =>
           options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
          

           );

            services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection")

           ));

            

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
           // app.Properties.Add("SendFromEmail", Configuration.GetConnectionString("SendFromEmail"));
            //app.UseHangfireServer();
            //app.UseHangfireDashboard();
            app.UseHttpsRedirection();
            app.UseMvc();
            //Only used for test purposes
            //RecurringJob.AddOrUpdate(() => SpocData.SmsOptOutData(), Cron.Daily,TimeZoneInfo.Local);
            //Send life support messages every 10 minutes between the hours of 8am to 8pm each day local time
            //RecurringJob.AddOrUpdate(() => SpocData.LoadLsData(), "*/10 8-20 * * *",,TimeZoneInfo.Local);
            //Send Automated network Status messages every 10 minutes UTC
            //RecurringJob.AddOrUpdate(() => SpocData.LoadSpocData(), Cron.MinuteInterval(10));
            //Send Automated reboot messages every 8 minutes UTC
            //RecurringJob.AddOrUpdate(() => SpocData.RebootData(), Cron.MinuteInterval(8));
            //Send last 24 hour Nar Field messages on tuesday through to friday every morning at 7am local time
            //RecurringJob.AddOrUpdate(() => SpocData.LoadNar24Data(), "0 7 * * tue,wed,thu,fri", TimeZoneInfo.Local);
            //Send last 72 hour Nar Field messages on Monday morning at 7am local time
            //RecurringJob.AddOrUpdate(() => SpocData.LoadNar72Data(), "0 7 * * mon", TimeZoneInfo.Local);
            //Send Cartographer Patch messages Monday to Friday morning at 8:30am local time
            //RecurringJob.AddOrUpdate(() => SpocData.PatchData(), "30 8 * * mon,tue,wed,thu,fri", TimeZoneInfo.Local);
            //Send Cartographer network Error messages Monday to Friday morning at 8:35am local time
            //RecurringJob.AddOrUpdate(() => SpocData.CartographerData(), "35 8 * * mon,tue,wed,thu,fri", TimeZoneInfo.Local);
        }
    }
}
