﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
namespace AutoSms.Models
{

    public class AutoSmsContext : DbContext
    {
        public AutoSmsContext()
        {
        }

        public AutoSmsContext(DbContextOptions<AutoSmsContext> options)
            : base(options)
        {
            Database.SetCommandTimeout(9000);
        }
        public DbSet<Spoc> Spocs { get; set; }
        public DbSet<EmailManager> EmailManager { get; set; }
        public DbSet<Ipart> Iparts { get; set; }
        public DbSet<DepotTranslation> DepotTranslations { get; set; }
        public DbSet<CustomerContact> CustomerContacts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           

           

        }

    }
}
