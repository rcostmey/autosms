﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class PersonMember
    {
        public string IdentityProviderId { get; set; }
        public string EmailAddress { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string DisplayName { get; set; }
        public string EmployeeId { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Department { get; set; }
        public string Manager { get; set; }
        public string Mobile { get; set; }
    }
}
