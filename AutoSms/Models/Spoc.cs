﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AutoSms.Models
{
    public class Spoc
    {
        private DateTime _DateCreated = DateTime.Now;
        [Key]
        public int SpocId { get; set; }
        public string INCIDENT_ID { get; set; }
        public string ACCESS_ISSUE { get; set; }
        public string AGE { get; set; }
        public string ASSN { get; set; }
        public string CANC { get; set; }
        public string CREATION_DATE { get; set; }
        public string CUST { get; set; }
        public string DESCRIPTION { get; set; }
        public string DISP { get; set; }
        public string EST_REST_DATE { get; set; }
        public string FAULT { get; set; }
        public string FAULT_COMMENT { get; set; }
        public string FEEDER { get; set; }
        public string FIRST_SWITCHING_TIME { get; set; }
        public string HRS_SINCE_SUPP_UPDATE { get; set; }
        public string INCIDENT_REF { get; set; }
        public string INC_CATEGORY_NAME { get; set; }
        public string JOB_ID { get; set; }
        public string LAST_UPDATE { get; set; }
        public string LS_COUNT { get; set; }
        public string NUM_OF_CALLS { get; set; }
        public string OPERATING_ZONE { get; set; }
        public string OUTSTANDING_CRIT_CALLS { get; set; }
        public string OVERNIGHT { get; set; }
        public string READY_TO_COMPLETE { get; set; }
        public string REGION { get; set; }
        public string RES_ETA { get; set; }
        public string RES_NAME { get; set; }
        public string RES_STATUS { get; set; }
        public string SECONDARY_ALIAS { get; set; }
        public string STATE { get; set; }
        public string SUPP_MESSAGE { get; set; }
        public string SWITCHING { get; set; }
        public string TYPE { get; set; }
        public string WAS_PRINTED { get; set; }
        public string ZONE_SUB { get; set; }
        public string OSC { get; set; }
        public string SMS_ID { get; set; }
        public string MaxCustomers { get; set; }
        public string AdditionalMessage { get; set; }
        public string SentStatus { get; set; }
        public string RestoreStatus { get; set; }
        public string CurrentStpis { get; set; }
        public string ContactList { get; set; }
        public string RegionTotal { get; set; }
        public string BusTotal { get; set; }
        public bool BussinessRule { get; set; }
        public DateTime DateUpdated { get { return _DateCreated; } set { _DateCreated = value; } }



    }
}
