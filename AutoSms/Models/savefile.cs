﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Specialized;
using Microsoft.AspNetCore.Hosting;
using System.Text;

namespace AutoSms.Models
{
    public class Savefile
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public Savefile(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;

        }
        public void SaveSmsOptOut(string filename, string _content)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string path = contentRootPath + "/" + filename;

            if (System.IO.File.Exists(path))
            {
                // Note that no lock is put on the 
                // file and the possibility exists 
                // that another process could do 
                // something with it between 
                // the calls to Exists and Delete.
                System.IO.File.Delete(path);
            }

            // Create the file. 
            using (FileStream fs = System.IO.File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(_content);
                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }

        }
    }
}
