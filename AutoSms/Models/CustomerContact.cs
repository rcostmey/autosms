﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AutoSms.Models
{
    public class CustomerContact
    {
        private DateTime _DateCreated = DateTime.Now;

        [Key]
        public int Id { get; set; }
        public string IncidentId { get; set; }
        public string HistoryTag { get; set; }
        public string IncidentRef { get; set; }
        public string LastUpdate { get; set; }
        public string EstRestDate { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string ProNumber { get; set; }
        public string Classification { get; set; }
        public string Mobile { get; set; }
        public string AdditionalMessage { get; set; }
        public DateTime SentStatus { get; set; }
        public DateTime RestoreStatus { get; set; }
        public string ContactList { get; set; }
        public bool Restored { get; set; }
        public DateTime UpdateStatus { get; set; }
        public DateTime DateUpdated { get { return _DateCreated; } set { _DateCreated = value; } }

    }
}
