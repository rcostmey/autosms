﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class UnplannedCustomers
    {
        public string INCIDENT_REF { get; set; }
        public string INCIDENT_ID { get; set; }
        public string Substation { get; set; }
        public string DIST_SUB { get; set; }
        public string Pof_Premise_No { get; set; }
        public string Sub_Name { get; set; }
        public string INC_TYPE_ID { get; set; }
        public string INC_CATEGORY_ID { get; set; }
        public string STATE { get; set; }
        public string METER_NO_ { get; set; }
        public string PCE_Premnum { get; set; }
        public string Premise_Address { get; set; }
        public string HOMEPHONE { get; set; }
        public string CELLPHONE { get; set; }
        public string DEBTORNUM { get; set; }
        public string CONSNUM { get; set; }
        public string STATUS_CONS { get; set; }
        public string Consumer_Name { get; set; }
        public string Consumer_Address { get; set; }
        public string MAIN_SUBSTATION { get; set; }
        public string NMI { get; set; }
        public string FRMP { get; set; }
        public string BRANCH_CODE { get; set; }
        public string MAILTOPREM { get; set; }
        public string DEPOT_CODE { get; set; }
        public string LIFE_SUPPORT { get; set; }
        public string CRITICAL_LOAD { get; set; }
        public string SMS_OPT_OUT { get; set; }

    }
}
