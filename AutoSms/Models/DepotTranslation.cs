﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AutoSms.Models
{
    public class DepotTranslation
    {

        [Key]
        public int Id { get; set; }
        public string OZ_NAME { get; set; }
        public string DISTRICT { get; set; }
        public string REGION { get; set; }
        public string OSC { get; set; }
        public string label { get; set; }
        public string SMS_ID { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Coordinates { get; set; }
       

    }
}
