﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class Patch
    {
        public string PATCH { get; set; }
        public string TEXT { get; set; }
        public string COMPONENT_ALIAS { get; set; }
        public string WORLD { get; set; }
    }
}
