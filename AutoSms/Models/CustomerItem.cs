﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class CustomerItem
    {
        public string ADDRESS { get; set; }
        public string CLASSIFICATION { get; set; }
        public string INCIDENT_ID { get; set; }
        public string LINK_REFERENCE { get; set; }
        public string NAME { get; set; }
        public string PRO_NUMBER { get; set; }
        public string STATE { get; set; }
        public string SUBSTATION { get; set; }
        public string TELEPHONE_NO { get; set; }
    }
}
