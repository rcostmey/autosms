﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class NarSs
    {
        public string name { get; set; }
        public string DESCRIPTION { get; set; }
        public string LOCATION { get; set; }
        public string mobile_number { get; set; }
        public string nar_id { get; set; }
        public string start_date_time { get; set; }
        public string switching_schedule_no { get; set; }
            
    }
}
