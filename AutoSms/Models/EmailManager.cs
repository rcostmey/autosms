﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AutoSms.Models
{
    public class EmailManager
    {
        [Key]
        public int Id { get; set; }
        public string TotalCustomers { get; set; }
        public string TotalNorthernControlCustomers { get; set; }
        public string TotalSouthernControlCustomers { get; set; }
        public string TotalSouthernCustomers { get; set; }
        public string TotalNorthernCustomers { get; set; }
        public string TotalNorthCoastCustomers { get; set; }
        public string AdditionalMessage { get; set; }
        public string SentStatus { get; set; }
        public string RestoreStatus { get; set; }
        public string ContactList { get; set; }
        public bool IsDelayed { get; set; }
        public DateTime DateUpdated { get; set; }

    }
}
