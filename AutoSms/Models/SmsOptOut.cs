﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class SmsOptOut
    {
        public string PREMNUM { get; set; }
        public string CONSUMER_NAME { get; set; }
        public string SMS_OPT_OUT { get; set; }
      
    }
}
