﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AutoSms.Models
{
    public class Ipart
    {
        private DateTime _DateCreated = DateTime.Now;
        [Key]
        public int IpartId { get; set; }
        public string INCD_REF { get; set; }
        public string INCD_CREATED { get; set; }
        public string LOCATION { get; set; }
        public string OZ_NAME { get; set; }
        public string OSC { get; set; }
        public string STAFF_ASSIGNED { get; set; }
        public string CRITICAL_INFO { get; set; }
        public string REMARKS { get; set; }
        public string ContactList { get; set; }
        public bool Voice { get; set; }
        public string SentStatus { get; set; }
        public DateTime DateUpdated { get { return _DateCreated; } set { _DateCreated = value; } }
    }
}
