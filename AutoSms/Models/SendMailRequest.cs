﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class SendMailRequest
    {
       
            public string Recipient { get; set; }
            public string Cc { get; set; }
            public string Replyto { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
           
        
    }
}
