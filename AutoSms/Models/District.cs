﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class District
    {
        public string Name { get; set; }
        public string RegionNumber { get; set; }
        public string Councils { get; set; }
        public string DangerLevelToday { get; set; }
        public string DangerLevelTomorrow { get; set; }
        public string FireBanToday { get; set; }
        public string FireBanTomorrow { get; set; }
    }
}
