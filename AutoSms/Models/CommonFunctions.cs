﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class CommonFunctions
    {
        public static string TfbVic()
        {
            var items =  Startup.Configuration.GetConnectionString("TfbVic");
           
            return items;
        }

        public static string TfbQld()
        {
            var items = Startup.Configuration.GetConnectionString("TfbQld");

            return items;
        }

        public static async Task<String> ProcessWhispir(string Tostr, string subtext, string bdytext, string voice)
        {

            String stringTask = string.Empty;
            HttpClient client = new HttpClient();
            string url = "http://reactweblive/api/messaging/sendmessage";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            

             var content = new StringContent("{\"ClientKey\": \"A563FE25-00A6-4A0F-BC69-9BD52ACFD80C\",\"To\": \"" + Tostr + "\",\"Subject\": \"" + subtext + "\",\"Body\": \"" + bdytext + "\",\"SendSMSAsVoiceMessage\": " + voice + "}", Encoding.UTF8, "application/json");
           
            HttpResponseMessage response = await client.PostAsync(url, content);
            if (response.IsSuccessStatusCode)
            {
                stringTask = await response.Content.ReadAsStringAsync();

            }

            return stringTask;

        }

       

            public static string DateFormat(string startTime)
        {
            string result = string.Empty;
            if (startTime != "")
            {
                if (startTime.IndexOf(".") != -1)
                { startTime = startTime.Substring(0, startTime.IndexOf(".")); }

                DateTime convertedDate = DateTime.Parse(startTime);

                result = convertedDate.ToLongDateString() + " " + convertedDate.ToShortTimeString();
            }


            return result;
        }

        public static async Task<String> ProcessUrl(string _address, bool IsJson, string ticket)
        {

            var handler = new HttpClientHandler() {
                DefaultProxyCredentials = new NetworkCredential(Startup.Configuration.GetConnectionString("Username"), Startup.Configuration.GetConnectionString("Credentials"), "corp")
            };
            HttpClient client = new HttpClient(handler);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.UserAgent.ParseAdd("Chickenman");

            if (IsJson)
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
            if (ticket != "")
            {
                client.DefaultRequestHeaders.Add("Ticket", ticket);
            }
            String stringTask = null;
            HttpResponseMessage response = await client.GetAsync(_address);
            if (response.IsSuccessStatusCode)
            {
                stringTask = await response.Content.ReadAsStringAsync();

            }
            else
            {
                stringTask = response.StatusCode.ToString();
            }

            return stringTask;

        }

    }
}
