﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class Feeder
    {
        public string CUSTOMERS { get; set; }
        public string FEEDER { get; set; }
        public string ZONE { get; set; }
        
    }
}
