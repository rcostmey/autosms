﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoSms.Models
{
    public class Stpis
    {
        public string FEEDER { get; set; }
        public string INCIDENT_ID { get; set; }
        public string INCIDENT_REF { get; set; }
        public DateTime INCCREATION_DATE { get; set; }
        public DateTime LOGGED_TIME { get; set; }
        public int NUM_OFF_SUPPLY { get; set; }
        public int NUM_ON_SUPPLY { get; set; }
    }
}
