﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using System.Xml.XPath;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Data.SqlClient;


namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NswRfsController : ControllerBase
    {
        private const string whispirTitle = "SYS Incident Notification Api : ";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AutoSmsContext _context;
        //private readonly string testMobile = "0447693137";
        private DateTime _Now = DateTime.Now;
     

        public NswRfsController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;

        }

        [HttpGet]
        public IActionResult GetTfb()
        {


            string ItemString = string.Empty;
            string url = "http://www.rfs.nsw.gov.au/feeds/fdrToban.xml";
            ItemString = CommonFunctions.ProcessUrl(url, true, "").GetAwaiter().GetResult();

            XmlDocument doc = new XmlDocument();
            try {
                doc.LoadXml(ItemString);
                var nodes = doc.ChildNodes;
                var Districts = nodes[nodes.Count - 1];
                ItemString = JsonConvert.SerializeObject(Districts);
                string pattern = @"\[.+\]";
                string result = string.Empty;
                foreach (Match match in Regex.Matches(ItemString, pattern))
                result += match.Value;
                
                List<District> items = JsonConvert.DeserializeObject<List<District>>(result);
                return Ok(items);
            }
            catch (Exception ex) { ItemString = ex.Message; }
           
            return Ok(ItemString);
        }
    }
}