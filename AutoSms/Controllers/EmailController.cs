﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using AutoSms.Models;
using AutoSms;
using Microsoft.IdentityModel.Protocols;
using System.Net;

namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        [HttpPost]
        [ActionName("sendmail")]
        public IActionResult ProcessAuthEmail(SendMailRequest mailRequest)
        {
            MailMessage msg = new MailMessage();
            string[] emailAddress = mailRequest.Recipient.Split("[,;]");
            foreach (string currentEmailAddress in emailAddress)
            {
                msg.To.Add(new MailAddress(currentEmailAddress.Trim()));
            }
            // Separate the cc array , if not null
            string[] ccAddress = null;

            if (mailRequest.Cc != null)
            {
                ccAddress = mailRequest.Cc.Split("[,;]");
                foreach (string currentCCAddress in ccAddress)
                {
                    msg.CC.Add(new MailAddress(currentCCAddress.Trim()));
                }
            }

            // Include the reply to if not null
            if (mailRequest.Replyto != null)
            {
               // msg.ReplyToList.Add(new MailAddress(mailRequest.Replyto));
            }

            msg.IsBodyHtml = true;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.From = new MailAddress("rick.costmeyer@essentialenergy.com.au", "SysOps");
            msg.Subject = mailRequest.Subject;
            msg.Body = mailRequest.Body;

            SmtpClient client = new SmtpClient("mailr1.corp.utilitiesnsw.com.au");
           



            try
            {
                client.Send(msg);
                msg.Dispose();
              
                return Ok("Mail Sent");
            }
            catch (Exception e)
            {
                return Ok(e.Message); 
            }

        }
    }
}