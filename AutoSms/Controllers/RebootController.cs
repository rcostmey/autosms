﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;

namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RebootController : ControllerBase
    {
        private const string url = "http://webviewnorth/webview/custom/rtu_reboot.pl";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AutoSmsContext _context;
        private readonly string testMobile = "rick.costmeyer@essentialenergy.com.au";//"0447693137";
        public RebootController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        public string MobilesReboot(List<Reboot> lst)
        {

            
            string Mobiles = string.Empty;
            

            foreach (var item in lst)
            {
                    try
                    {
                        if (Mobiles.IndexOf(item.ATTRIBUTE_VALUE.Trim()) == -1 && item.ATTRIBUTE_VALUE.Trim() != "")
                        { Mobiles += item.ATTRIBUTE_VALUE.Trim() + ";"; }
                    }
                    catch (Exception ex) { Console.Write(ex.Message); }
                
            }

            Mobiles = Mobiles.Replace(" ", "");
            return Mobiles.Substring(0, Mobiles.LastIndexOf(";"));
        }

        [HttpGet]
        public IActionResult GetRebootItems()
        {
            DataSet s = new DataSet();
            string ItemString = string.Empty;
            s = new DataSet();
            try
            {
                s.ReadXml(url);
            }
            catch (Exception ex) { Console.Write(ex.Message); }
            //convert table into a json string (equivalent to JSON.stringify)
            if (s.Tables.Count != 0)
            {


                ItemString = JsonConvert.SerializeObject(s.Tables[0]);

                List<Reboot> items = JsonConvert.DeserializeObject<List<Reboot>>(ItemString);

                
                string Body = string.Empty;
                Body += ".";
                string Subject = "Reboot";

                var whispir = CommonFunctions.ProcessWhispir(MobilesReboot(items), Subject, Body, "false").GetAwaiter().GetResult();
                whispir = CommonFunctions.ProcessWhispir(testMobile, Subject, MobilesReboot(items), "false").GetAwaiter().GetResult();
                return Ok(items);
            }

            return Ok();
        }
    }
}