﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LSController : ControllerBase
    {
        private const string whispirTitle = "SYS Incident Notification Api : ";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AutoSmsContext _context;
        private readonly string testMobile = "0447693137";
        private DateTime _Now = DateTime.Now;

        public LSController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
           
        }

        public string DateFormat(string startTime)
        {
            string result = string.Empty;
            if (startTime != "")
            {
                if (startTime.IndexOf(".") != -1)
                { startTime = startTime.Substring(0, startTime.IndexOf(".")); }

                DateTime convertedDate = DateTime.Parse(startTime);

                result = convertedDate.ToLongDateString() + " " + convertedDate.ToShortTimeString();
            }


            return result;
        }

        void SaveSmsOptOut(string filename, string _content)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string path = contentRootPath + "/" + filename;
            
            if (System.IO.File.Exists(path))
            {
                // Note that no lock is put on the 
                // file and the possibility exists 
                // that another process could do 
                // something with it between 
                // the calls to Exists and Delete.
                System.IO.File.Delete(path);
            }

            // Create the file. 
            using (FileStream fs = System.IO.File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(_content);
                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }

        }

        static async Task<String> ProcessSmsOptOut(string INCIDENT_REF)
        {
            String stringTask = string.Empty;
            try
            {
                SqlConnection conn = new SqlConnection
                {
                    ConnectionString =
                "Data Source= PCE9ODSMSQLLIVE;" +
                "Initial Catalog=ODSPOF_AUXDB;" +
                "Integrated Security=True;"
                };

                //string sql = "SELECT [PREMNUM],[CONSUMER_NAME],[SMS_OPT_OUT] FROM [REPORTING].[VW_R128_CUSTOMER_DATA_EXTRACT_PART1] WHERE([REPORTING].[VW_R128_CUSTOMER_DATA_EXTRACT_PART1].[SMS_OPT_OUT] = 'Y');";
                //using (SqlCommand command = new SqlCommand(sql, conn))
                // {
                await conn.OpenAsync();
                string sql = "SELECT * FROM [REPORTING].[VW_R129_UNPLANNED_INCIDENT_CUSTOMER_DATA_EXTRACT] WHERE(([INCIDENT_REF] = '" + INCIDENT_REF + "'));";
                SqlCommand command = new SqlCommand(sql, conn)
                {

                    CommandTimeout = 3000
                };
                var adapter = new SqlDataAdapter(command);

                // use the connection here
                //string sql = "SELECT * FROM [REPORTING].[VW_R128_CUSTOMER_DATA_EXTRACT_PART1] WHERE( [REPORTING].[VW_R128_CUSTOMER_DATA_EXTRACT_PART1].[SMS_OPT_OUT] = 'Y');";
                //string sql = "SELECT * FROM [REPORTING].[R128_CUSTOMER_DATA_EXTRACT] WHERE([REPORTING].[R128_CUSTOMER_DATA_EXTRACT].[PREMNUM] in (1948588,495165,2572801,597926,597497,597500,597600));";
                //string sql = "select * from [REPORTING].[VW_R128_INCIDENT_CUSTOMER_DATA_EXTRACT] where(INCIDENT_REF='INCD-69759-g');";
                //string sql = "select top 5 * from [REPORTING].[VW_R468_CDN_SMS_OPT_OUT];";
                //SqlCommand command = new SqlCommand(sql, conn);

                // var adapter = new SqlDataAdapter(command);
                 var s = new DataSet();
                 adapter.Fill(s);

                conn.Close();
                conn.Dispose();
                stringTask = string.Empty;
                if (s.Tables.Count != 0)
                {
                stringTask = JsonConvert.SerializeObject(s.Tables[0]);
                   


                }
                //}

            }
            catch (Exception ex)
            { stringTask = ex.Message; }



            return stringTask;
        }



        static async Task<String> ProcessCSV(string _address,string filename, byte[] _content)
        {

            String stringTask = string.Empty;
            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent("Upload"))
                {
                    content.Add(new StreamContent(new MemoryStream(_content)), filename.Substring(0, filename.IndexOf(".")), filename);

                    using (
                       var message =
                           await client.PostAsync(_address, content))
                    {
                        stringTask = await message.Content.ReadAsStringAsync();

                    }
                }
            }

            return stringTask;

        }
        static async Task<String> ProcessCsvToWhispir(string _address,string ResourceId, string WorkspaceId, string MessageTemplateId, string Mobile)
        {
            var content = new StringContent("{\"ClientKey\": \"A563FE25-00A6-4A0F-BC69-9BD52ACFD80C\",\"ResourceId\": \"" + ResourceId + "\",\"MobileFieldName\": \"" + Mobile + "\",\"WorkspaceId\": \"" + WorkspaceId + "\",\"MessageTemplateId\": \"" + MessageTemplateId + "\",\"CheckSentStatus\": true}", Encoding.UTF8, "application/json"); 
            String stringTask = null;
            HttpClient client = new HttpClient();
            string url = _address;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            HttpResponseMessage response = await client.PostAsync(url, content);
            if (response.IsSuccessStatusCode)
            {
                stringTask = await response.Content.ReadAsStringAsync();

            }

            return stringTask;

        }

            string CreateCsv(List<CustomerContact> contacts)
            {
            string header = "HISTORY TAG,INCIDENT REF.,DESCRIPTION,LAST UPDATED,FAULT,ETR,NAME,ADDRESS,PRO. NUMBER,CLASSIFICATION,MOBILE,EMAIL\n";
            string values = string.Empty;
            foreach (var contact in contacts)
            {
                values += "\"" + contact.HistoryTag + "\",";
                values += "\"" + contact.IncidentRef+ "\",";
                values += "\"" + contact.Address + "\",";
                values += "\"" + _Now.ToLongDateString() + " " + _Now.ToShortTimeString() + "\",";
                values += "\"" + "Broken X-arm" + "\",";
                if(contact.EstRestDate != "")
                { values += "\"" + DateFormat(contact.EstRestDate) + "\","; }
                else
                { values += "\"" + string.Empty + "\","; }
                values += "\"" + contact.Name + "\",";
                values += "\"" + contact.Address + "\",";
                values += "\"" + contact.ProNumber + "\",";
                values += "\"" + contact.Classification + "\",";
                values += "\"" + contact.Mobile + "\",";
                values += "\"" + "rick.costmeyer@essentialenergy.com.au" + "\"";
                values += "\n";
            }

             return header + values;
        }

        string  SaveCsv(string filename, string Data)
        {

            string output = string.Empty;
          

                
                Byte[] info = new UTF8Encoding(true).GetBytes(Data);
                
                string MessageTemplateId = string.Empty;
                switch (filename)
                {
                    case "RestoreContacts.csv": MessageTemplateId = "157315F837F72F87";
                    break;
                    case "UpdateContacts.csv": MessageTemplateId = "1DFFB3578C1B6CA6";
                    break;
                    case "NewContacts.csv": MessageTemplateId = "9DA6DCC5EDAA3B64";
                    break;
                }
               

                string result = ProcessCSV("http://reactweblive/api/messaging/uploadbulkcsv", filename, info).GetAwaiter().GetResult();
                output = ProcessCsvToWhispir("http://reactweblive/api/messaging/sendbulkmessage", result.Replace("\"",""), "05A9BF7608BE6113", MessageTemplateId, "MOBILE").GetAwaiter().GetResult();
               
         
            return output;
        }

        

        public Boolean TimeOff(string startTime, int minutes)
        {
            Boolean result = false;

            if (startTime.IndexOf(".") != -1)
            { startTime = startTime.Substring(0, startTime.IndexOf(".")); }

            DateTime convertedDate = DateTime.Parse(startTime);
            long diff = _Now.Ticks - convertedDate.Ticks;
            TimeSpan elapsedSpan = new TimeSpan(diff);
            if (elapsedSpan.TotalMinutes >= minutes) { result = true; }
            return result;
        }

        void ProcessRestore(List<Spoc> spocFilter)
        {
            var existingCustomerContacts = _context.CustomerContacts.Where(i => (!i.Restored));

            List<CustomerContact> RestoreCustomerContacts = new List<CustomerContact>();

            foreach (var incident in existingCustomerContacts)
            {

                if (spocFilter.Any(o => o.INCIDENT_ID == incident.IncidentId)) { }
                else
                {
                    

                    var IncidentsRestore = _context.CustomerContacts.Where(b => b.IncidentId == incident.IncidentId);
                    foreach (var item in IncidentsRestore)
                    {
                        item.HistoryTag = "Restore";
                        item.Restored = true;
                        item.RestoreStatus = _Now;
                        item.DateUpdated = _Now;
                        RestoreCustomerContacts.Add(item);
                    }

                }
            }

            if (RestoreCustomerContacts.Count != 0)
            {
                _context.CustomerContacts.UpdateRange(RestoreCustomerContacts);
                _context.SaveChanges();
                string filename = SaveCsv("RestoreContacts.csv", CreateCsv(RestoreCustomerContacts)); 
            }


        }

        IEnumerable<CustomerItem> CustomerList(string incidentId)
        {
            IEnumerable<CustomerItem> customerItemFilter = new List<CustomerItem>();
           
            string url = "http://webviewhomp1/webview/custom/log_storm_updates/GetCustList.pl?incident_id=" + incidentId;
            DataSet s = new DataSet();
            string ItemString = string.Empty;
            s = new DataSet();
            try
            {
                s.ReadXml(url);
            }
            catch (Exception ex) { Console.Write(ex.Message); }

            if (s.Tables.Count != 0)
            {
                ItemString = JsonConvert.SerializeObject(s.Tables[0]);

                List<CustomerItem> customerItems = JsonConvert.DeserializeObject<List<CustomerItem>>(ItemString);
               customerItemFilter = customerItems.Where(b => (b.CLASSIFICATION == "MED" || b.CLASSIFICATION == "CRITL") && b.STATE== "Off");


            }

            return customerItemFilter;

        }
        IEnumerable<UnplannedCustomers> CustomerDetails(string incidentRef)
        {
            string ItemString = string.Empty;
            IEnumerable<UnplannedCustomers> customerItemFilter = new List<UnplannedCustomers>();
            ItemString = ProcessSmsOptOut(incidentRef).GetAwaiter().GetResult();
            List<UnplannedCustomers> customerItems = JsonConvert.DeserializeObject<List<UnplannedCustomers>>(ItemString);
            customerItemFilter = customerItems.Where(b => (b.LIFE_SUPPORT == "Y" || b.CRITICAL_LOAD == "Y") && b.STATE == "DEAD" && b.SMS_OPT_OUT=="N");
            return customerItemFilter;
        }

        [HttpGet]
        public IActionResult GetLifeSupportCustomers()
        {
            //string contentRootPath = _hostingEnvironment.ContentRootPath;
            //string SmsOptOutJson = System.IO.File.ReadAllText(contentRootPath + "/SmsOptOut.json");

            //List<SmsOptOut> SmsOptOuts = JsonConvert.DeserializeObject<List<SmsOptOut>>(SmsOptOutJson);


            string url = "http://webviewhomp1/webview/custom/log_storm_updates/hv_storm_dashboard.pl";
            DataSet s = new DataSet();
            string ItemString = string.Empty;
            s = new DataSet();
            try
            {
                s.ReadXml(url);
            }
            catch (Exception ex) { Console.Write(ex.Message); }

            List<CustomerContact> NewCustomerContacts = new List<CustomerContact>();
            List<CustomerContact> UpdateCustomerContacts = new List<CustomerContact>();

            if (s.Tables.Count != 0)
            {
                ItemString = JsonConvert.SerializeObject(s.Tables[0]);

                List<Spoc> spocs = JsonConvert.DeserializeObject<List<Spoc>>(ItemString);
                var spocFilter = spocs.GroupBy(o => new { o.INCIDENT_ID }).Select(o => o.FirstOrDefault()).Where(i => (Int32.Parse(i.LS_COUNT) >0 && TimeOff(i.CREATION_DATE, 10)));
                foreach (var incident in spocFilter)
                {
                    if (_context.CustomerContacts.Any(o => o.IncidentId == incident.INCIDENT_ID))
                    {
                        var UpdateGroup = _context.CustomerContacts.Where(b => b.IncidentId == incident.INCIDENT_ID && b.EstRestDate != incident.EST_REST_DATE);

                        foreach (var item in UpdateGroup)
                        {
                            item.EstRestDate = incident.EST_REST_DATE;
                            item.HistoryTag = "Update";
                            item.UpdateStatus = _Now;
                            item.DateUpdated = _Now;
                            UpdateCustomerContacts.Add(item);
                        }

                       

                    }
                    else
                    {


                        foreach (var Contact in CustomerDetails(incident.INCIDENT_REF))
                        {
                            string TelephoneNo = string.Empty;
                            if (Contact.HOMEPHONE != null )
                            { TelephoneNo = Contact.HOMEPHONE.Replace("-", "").Replace("\\s", "") + ";"; }
                            if (Contact.CELLPHONE != null)
                            { TelephoneNo += Contact.CELLPHONE.Replace("-", "").Replace("\\s", "") + ";"; }
                           
                            Match m = Regex.Match(TelephoneNo, "04[0-9]{8}", RegexOptions.IgnoreCase);
                            if (m.Success)
                            {
                                string classification = string.Empty;
                                if (Contact.LIFE_SUPPORT == "Y")
                                { classification = "LS"; }
                                else
                                { classification = "CRTL"; }
                                var newItem = new CustomerContact
                                {
                                    HistoryTag = "New",
                                    Address = Contact.Premise_Address,
                                    Name = Contact.Consumer_Name,
                                    Classification = classification,
                                    IncidentId = incident.INCIDENT_ID,
                                    IncidentRef = incident.INCIDENT_REF,
                                    EstRestDate = incident.EST_REST_DATE,
                                    Mobile = testMobile,
                                    ProNumber = Contact.Pof_Premise_No,
                                    ContactList = m.Value,
                                    SentStatus = _Now
                                };
                                NewCustomerContacts.Add(newItem);
                            }
                            
                        }
                         
                       
                    }
                }
                string filename = string.Empty;
                if (UpdateCustomerContacts.Count != 0)
                {
                    _context.CustomerContacts.UpdateRange(UpdateCustomerContacts);
                    _context.SaveChanges();
                    filename = SaveCsv("UpdateContacts.csv", CreateCsv(NewCustomerContacts)); 
                }
                if (NewCustomerContacts.Count != 0)
                {
                    _context.CustomerContacts.AddRange(NewCustomerContacts);
                    _context.SaveChanges();
                    filename = SaveCsv("NewContacts.csv", CreateCsv(NewCustomerContacts)); 
                }
                ProcessRestore(spocs);
                return Ok(NewCustomerContacts);
            }
        
            return Ok();
        }
    }
}