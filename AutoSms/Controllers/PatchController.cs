﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;

namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatchController : ControllerBase
    {
        private const string url = "http://webviewhomp1/webview/custom/PatchIntroduction.pl";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AutoSmsContext _context;
        private readonly string emailList = "rick.costmeyer@essentialenergy.com.au;poweron.updates@essentialenergy.com.au";

        public PatchController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        [HttpGet]
        public IActionResult GetPatchItems()
        {
            DataSet s = new DataSet();
            string ItemString = string.Empty;
            s = new DataSet();
            try
            {
                s.ReadXml(url);
            }
            catch (Exception ex) { ItemString = ex.Message; }
            //convert table into a json string (equivalent to JSON.stringify)
            if (s.Tables.Count != 0)
            {


                ItemString = JsonConvert.SerializeObject(s.Tables[0]);
                string Body = string.Empty;
                string Subject = string.Empty;
                var whispir = string.Empty;
                List<Patch> Patchs = JsonConvert.DeserializeObject<List<Patch>>(ItemString);

                foreach (var Patch in Patchs)
                {
                    Body = string.Empty;
                    Body += Patch.PATCH + ";" + Patch.TEXT + ";" + Patch.COMPONENT_ALIAS + "; In World : " + Patch.WORLD;

                    Subject = "Urgent Patch Introduction Report";
                    whispir = CommonFunctions.ProcessWhispir(emailList, Subject, Body, "false").GetAwaiter().GetResult();
                }
                return Ok(Patchs);
            }

            return Ok(ItemString);
        }
    }
}