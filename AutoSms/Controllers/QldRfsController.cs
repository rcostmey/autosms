﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using System.Xml.XPath;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QldRfsController : ControllerBase
    {
        private const string whispirTitle = "SYS Incident Notification Api : ";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AutoSmsContext _context;
        //private readonly string testMobile = "0447693137";
        private DateTime _Now = DateTime.Now;


        public QldRfsController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;

        }

        [HttpGet]
        public IActionResult GetQldTfb()
        {


            string ItemString = string.Empty;
            string url = "https://www.ruralfire.qld.gov.au/BushFire_Safety/Neighbourhood-Safer-Places/lgas/_layouts/15/listfeed.aspx?List=a4f237e1-b263-4062-a8e2-82774f87f01d&View=a0a7270f-6252-422c-96f2-d7088ae16ffe";
            ItemString = CommonFunctions.ProcessUrl(url, true, "").GetAwaiter().GetResult();

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(ItemString);
                var nodes = doc.ChildNodes;
                
                ItemString = JsonConvert.SerializeObject(nodes);
                string pattern = @"Fire Bans:.+},";
                string result = string.Empty;
               
                Match m = Regex.Match(ItemString, pattern, RegexOptions.IgnoreCase);
                result = "[{" + m.Value.ToString().Replace("},", "}]").Replace("Fire Bans:","FireBans:\"").Replace("Modified:", "\",Modified:\""); 
                 List<QldTfb> items = JsonConvert.DeserializeObject<List<QldTfb>>(result);
                return Ok(items);
            }
            catch (Exception ex) { ItemString = ex.Message; }

            return Ok(ItemString);
        }
    }
}