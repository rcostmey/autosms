﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;

namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NarController : ControllerBase
    {
        private const string url = "http://webviewhomp1/webview/custom/NAR_ss_attached_24.pl?Hours=24";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AutoSmsContext _context;
        private readonly string testMobile = "rick.costmeyer@essentialenergy.com.au";

        public NarController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        [HttpGet]
        public IActionResult GetNarItems()
        {
            DataSet s = new DataSet();
            string ItemString = string.Empty;
            s = new DataSet();
            try
            {
                s.ReadXml(url);
            }
            catch (Exception ex) { Console.Write(ex.Message); }
            //convert table into a json string (equivalent to JSON.stringify)
            if (s.Tables.Count != 0)
            {

                var whispir = string.Empty;
                ItemString = JsonConvert.SerializeObject(s.Tables[0]);

                List<NarSs> NarSss = JsonConvert.DeserializeObject<List<NarSs>>(ItemString);
                var Item = NarSss[0];
                string Body = string.Empty;
                string Subject = string.Empty;
                Body += "Switching Instruction " + Item.switching_schedule_no + " has been attached to NAR#" + Item.nar_id + " for your review.\n";
                Body += "DESCRIPTION : " + Item.DESCRIPTION + "\n";
                Body += "LOCATION : " + Item.LOCATION + "\n";
                Body += " Please do not reply to this automated SMS.\n";
                Subject = "NAR#" + Item.nar_id + " ; " + CommonFunctions.DateFormat(Item.start_date_time.ToString()) + " " + Item.name + ".";
                whispir = CommonFunctions.ProcessWhispir(testMobile, Subject, Body, "false").GetAwaiter().GetResult();
                foreach (var Nar in NarSss)
                {
                    Body = string.Empty;
                    Body += "Switching Instruction " + Nar.switching_schedule_no + " has been attached to NAR#" + Nar.nar_id + " for your review.\n";
                    Body += "DESCRIPTION : " + Nar.DESCRIPTION + "\n";
                    Body += "LOCATION : " + Nar.LOCATION + "\n";
                    Body += " Please do not reply to this automated SMS.\n";
                    Subject = "NAR#" + Nar.nar_id + " ; " + CommonFunctions.DateFormat(Nar.start_date_time.ToString()) + " " + Nar.name + ".";
                    whispir = CommonFunctions.ProcessWhispir(Nar.mobile_number.Replace(" ",""), Subject, Body, "false").GetAwaiter().GetResult();
                }
                    return Ok(NarSss);
            }

                return Ok();
        }


    }
}