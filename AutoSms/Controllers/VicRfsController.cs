﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using System.Xml.XPath;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace AutoSms.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VicRfsController : ControllerBase
    {
       
            private const string whispirTitle = "SYS Incident Notification Api : ";
            private readonly IHostingEnvironment _hostingEnvironment;
            private readonly AutoSmsContext _context;
            //private readonly string testMobile = "0447693137";
            private DateTime _Now = DateTime.Now;


            public VicRfsController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
            {
                _hostingEnvironment = hostingEnvironment;
                _context = context;

            }
            string FireValue(string input,string region,string end,bool first)
            {
            string val = string.Empty;
            if (first)
            {
                val = input.Substring(input.IndexOf(region) + region.Length);
                val = val.Substring(0, val.IndexOf(end));
            }
            else
            {
                val = input.Substring(input.LastIndexOf(region) + region.Length);
                val = val.Substring(0, val.IndexOf(end));
            }

            return val;
            }

            [HttpGet]
            public IActionResult GetVicTfb()
            {

                
                string ItemString = string.Empty;
                ItemString = CommonFunctions.TfbQld();
                List<District> Tfbs = JsonConvert.DeserializeObject<List<District>>(ItemString);
                
            return Ok(Tfbs);
            }
        
    }
}