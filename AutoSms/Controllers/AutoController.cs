﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;

namespace AutoSms.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AutoController : ControllerBase
    {
        private const string whispirTitle = "SYS Incident Notification Api : ";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AutoSmsContext _context;
        private readonly string defaultContacts = "noms.sms@essentialenergy.com.au;NOTO-South@essentialenergy.com.au;rick.costmeyer@essentialenergy.com.au;0439418357;0409812252;0448038648";
        private readonly string testMobile = "0447693137";//;0408479480;0408654186
        //private readonly string testEmail = "rick.costmeyer@essentialenergy.com.au;andrew.thrower@essentialenergy.com.au;ron.whalen@essentialenergy.com.au";
        //private readonly string testMobile = "rick.costmeyer@essentialenergy.com.au";
        private readonly string testEmail = "rick.costmeyer@essentialenergy.com.au";
        private DateTime _Now = DateTime.Now;
        private int _BusTotal = 0;
        private int _NTotal = 0;
        private int _STotal = 0;
        private int _NcTotal = 0;
        private int _NOscTotal = 0;
        private int _SOscTotal = 0;

        public AutoController(IHostingEnvironment hostingEnvironment, AutoSmsContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        


        static async Task<String> ProcessEmail(string Tostr, string subtext, string bdytext, string func)
        {

            String stringTask = null;
            HttpClient client = new HttpClient();
            string url = "http://nocr/spoc_portal/myjson.aspx";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Add("Ticket", "77FC7886-45CC-11E7-A61B-4A3281D1878A");

            var content = new StringContent("{\"Emailto\": \"" + Tostr + "\",\"EmailCC\": \"" + "" + "\",\"EmailSubject\": \"" + subtext + "\",\"EmailBody\": \"" + bdytext + "\",\"func\": " + func + "}", Encoding.UTF8, "application/json");
            //var result = await client.PostAsync(url, content);
            HttpResponseMessage response = await client.PostAsync(url, content);
            if (response.IsSuccessStatusCode)
            {
                stringTask = await response.Content.ReadAsStringAsync();

            }
            else
            {
                stringTask = response.ToString();
            }

            return stringTask;

        }


        static async Task<String> ProcessUrl(string _address)
        {


            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Ticket", "77FC7886-45CC-11E7-A61B-4A3281D1878A");
            String stringTask = null;//client.GetStringAsync(_address);
            HttpResponseMessage response = await client.GetAsync(_address);
            if (response.IsSuccessStatusCode)
            {
                stringTask = await response.Content.ReadAsStringAsync();

            }
            else
            {
                stringTask = response.ToString();
            }
            return stringTask;

        }

        public string EmailAddresses(List<Nom> noms)
        {

            string Emails = string.Empty;
            List<PersonMember> tmpMember = new List<PersonMember>();
            
            foreach (var nom in noms)
            {
                var Contacts = nom.PersonMembers.Where(i => (i.Title == "Community Relations Manager" || i.Title.Contains("Regional Manager") || i.Title == "Area Manager" || i.Title == "Corporate Media Manager"));
                foreach (var Contact in Contacts)
                {
                    try
                    {
                        if (Emails.IndexOf(Contact.EmailAddress.Trim()) == -1 && Contact.EmailAddress.Trim() != "")
                        { Emails += Contact.EmailAddress.Trim() + ";"; }
                    }
                    catch (Exception ex) { Console.Write(ex.Message); }
                }
            }
               
                return Emails.Substring(0, Emails.LastIndexOf(";"));
        }

        public string ContactsNoms(List<Nom> noms, string groupName)
        {

            var lst = noms.Where(b => b.Name == groupName);
            string Emails = string.Empty;
            List<PersonMember> tmpMember = new List<PersonMember>();

            foreach (var nom in lst)
            {
                var Contacts = nom.PersonMembers;
                foreach (var Contact in Contacts)
                {
                    try
                    {
                        if (Emails.IndexOf(Contact.EmailAddress.Trim()) == -1 && Contact.EmailAddress.Trim() != "")
                        { Emails += Contact.EmailAddress.Trim() + ";"; }

                    }
                    catch (Exception ex) { Console.Write(ex.Message); }
                    try
                    {
                        if (Emails.IndexOf(Contact.Mobile.Trim()) == -1 && Contact.Mobile.Trim() != "")
                        { Emails += Contact.Mobile.Trim() + ";"; }
                    }
                    catch (Exception ex) { Console.Write(ex.Message); }
                }
            }
            if(groupName == "IPART Reportable Incidents Group")
            {
                Emails += "rick.costmeyer@essentialenergy.com.au" + ";";
                Emails += "noms.sms@essentialenergy.com.au" + ";";
                Emails += "networkoperations.north@essentialenergy.com.au" + ";";
                Emails += "0439418357" + ";";
                Emails += "0409812252" + ";";
                Emails += "0448038648" + ";";
                // Justin Martin
                Emails += "0428270407" + ";";
                Emails += "Justin.Martin@essentialenergy.com.au" + ";";
            }
            Emails = Emails.Replace(" ", "");
            return Emails.Substring(0, Emails.LastIndexOf(";"));
        }

        public string EmailAddressesNoms(List<Nom> noms,string groupName)
        {

            var lst = noms.Where(b => b.Name == groupName);
            string Emails = string.Empty;
            List<PersonMember> tmpMember = new List<PersonMember>();

            foreach (var nom in lst)
            {
                var Contacts = nom.PersonMembers;
                foreach (var Contact in Contacts)
                {
                    try
                    {
                        if (Emails.IndexOf(Contact.EmailAddress.Trim()) == -1 && Contact.EmailAddress.Trim() != "")
                        { Emails += Contact.EmailAddress.Trim() + ";"; }
                    }
                    catch (Exception ex) { Console.Write(ex.Message); }
                }
            }

            return Emails.Substring(0, Emails.LastIndexOf(";"));
        }



        public string MobilesNoms(List<Nom> noms, string groupName)
        {

            var lst = noms.Where(b => b.Name == groupName);
            string Mobiles = string.Empty;
            List<PersonMember> tmpMember = new List<PersonMember>();

            foreach (var nom in lst)
            {
                var Contacts = nom.PersonMembers;
                foreach (var Contact in Contacts)
                {
                    try
                    {
                        if (Mobiles.IndexOf(Contact.Mobile.Trim()) == -1 && Contact.Mobile.Trim() != "")
                        { Mobiles += Contact.Mobile.Trim() + ";"; }
                    }
                    catch(Exception ex) { Console.Write(ex.Message); }
                }
            }

            Mobiles = Mobiles.Replace(" ", "");
            return Mobiles.Substring(0, Mobiles.LastIndexOf(";"));
        }

        public string EmailAddressesManagers(List<Nom> noms)
        {

            string Emails = string.Empty;
            List<PersonMember> tmpMember = new List<PersonMember>();

            foreach (var nom in noms)
            {
                var Contacts = nom.PersonMembers.Where(i => (i.Title.Contains("Regional Manager") || i.Title == "Manager System Control"));

                foreach (var Contact in Contacts)
                {
                    try
                    {
                        if (Emails.IndexOf(Contact.EmailAddress.Trim()) == -1 && Contact.EmailAddress.Trim() != "")
                        { Emails += Contact.EmailAddress.Trim() + ";"; }
                    }
                    catch (Exception ex) { Console.Write(ex.Message); }

                }
            }

            return Emails.Substring(0, Emails.LastIndexOf(";"));
        }

        public string MobileManagers(List<Nom> noms)
        {
           
            string Mobiles = string.Empty;
            List<PersonMember> tmpMember = new List<PersonMember>();

            foreach (var nom in noms)
            {
                var Contacts = nom.PersonMembers.Where(i => (i.Title.Contains("Regional Manager") || i.Title == "Manager System Control"));
                foreach (var Contact in Contacts)
                {
                    try
                    {
                        if (Mobiles.IndexOf(Contact.Mobile.Trim()) == -1 && Contact.Mobile.Trim() != "")
                        { Mobiles += Contact.Mobile.Trim() + ";"; }
                    }
                    catch (Exception ex) { Console.Write(ex.Message); }
                }
            }
            Mobiles = Mobiles.Replace(" ", "");
            return Mobiles.Substring(0, Mobiles.LastIndexOf(";"));
        }





        void ProcessFiveThousandPlus(List<Spoc> spocs, List<Nom> noms)
        {
        int BusTotal = 0;
        int NTotal = 0;
        int STotal = 0;
        int NcTotal = 0;
        int NOscTotal = 0;
        int SOscTotal = 0;

        string pattern = @"\d{10}";
        var Contacts = noms.Where(b => b.Name == "RecipientsInstant");
        
        string Mobiles = string.Empty;
        foreach (var nom in Contacts)
        {
                string input = nom.Contacts.ToString();

                foreach (Match match in Regex.Matches(input, pattern))
                    Mobiles += match.Value + ";";
        }

            var SumCustomersOff = spocs.GroupBy(o => new { o.INCIDENT_ID }).Select(o => o.FirstOrDefault()).Where(i => (i.TYPE == "HVN" && i.STATE == "Conf."));

            foreach (var incident in SumCustomersOff)
            {
                var DepotTrans = _context.DepotTranslations.SingleOrDefault(b => b.OZ_NAME == incident.OPERATING_ZONE);
                incident.REGION = DepotTrans.REGION;
                incident.SMS_ID = DepotTrans.SMS_ID;
                incident.OSC = DepotTrans.OSC;
                BusTotal += Int32.Parse(incident.CUST);
                switch (incident.REGION)
                {
                    case "Northern":
                        NTotal += Int32.Parse(incident.CUST);
                        break;
                    case "Southern":
                        STotal += Int32.Parse(incident.CUST);
                        break;
                    case "North Coast":
                        NcTotal += Int32.Parse(incident.CUST);
                        break;

                }

                if (incident.OSC == "PMQ") {NOscTotal += Int32.Parse(incident.CUST); } else {SOscTotal += Int32.Parse(incident.CUST); }


            }

            var existingReports = _context.EmailManager.Where(i => (i.RestoreStatus == "" || i.RestoreStatus == null) && i.IsDelayed == false);
            int reports = existingReports.Count();

            if (reports != 0)
            {
                if (BusTotal < 5000)
                {
                    foreach (var item in existingReports)
                    {
                        item.AdditionalMessage = "Significant Customers off supply has returned below 5000 at";
                        item.RestoreStatus = CommonFunctions.ProcessWhispir(Mobiles, whispirTitle + item.AdditionalMessage, "", "false").GetAwaiter().GetResult();
                    }

                    _context.EmailManager.UpdateRange(existingReports);
                    _context.SaveChanges();

                }
            }
            else
            {
                if (BusTotal >= 5000)
                {

                    EmailManager item = new EmailManager();
                    item.TotalCustomers = BusTotal.ToString();
                    item.TotalNorthernCustomers = NTotal.ToString();
                    item.TotalSouthernCustomers =  STotal.ToString();
                    item.TotalNorthCoastCustomers = NcTotal.ToString();
                    item.TotalNorthernControlCustomers = NOscTotal.ToString();
                    item.TotalSouthernControlCustomers = SOscTotal.ToString();
                    item.DateUpdated = _Now;
                    item.AdditionalMessage = "Significant Customers off supply has exceeded 5000";
                    item.SentStatus = CommonFunctions.ProcessWhispir(Mobiles, whispirTitle + item.AdditionalMessage, SignificantDetails(item), "false").GetAwaiter().GetResult();
                    _context.EmailManager.Add(item);
                    _context.SaveChanges();

                }

            }


        }

        void ProcessFiveThousandPlusDelayed(IEnumerable<Spoc> spocs, List<Nom> noms)
        {
            int BusTotal = 0;
            int NTotal = 0;
            int STotal = 0;
            int NcTotal = 0;
            int NOscTotal = 0;
            int SOscTotal = 0;

            string pattern = @"\d{10}";
            var Contacts = noms.Where(b => b.Name == "RecipientsDelayed");
            
            string Mobiles = string.Empty;
            foreach (var nom in Contacts)
            {
                string input = nom.Contacts.ToString();

                foreach (Match match in Regex.Matches(input, pattern))
                    Mobiles += match.Value + ";";
            }
            string Emails = string.Empty;
            foreach (var nom in Contacts)
            {
                Emails = nom.Contacts.ToString();

            }

            var SumCustomersOff = spocs;

            foreach (var incident in SumCustomersOff)
            {
                var DepotTrans = _context.DepotTranslations.SingleOrDefault(b => b.OZ_NAME == incident.OPERATING_ZONE);
                incident.REGION = DepotTrans.REGION;
                incident.SMS_ID = DepotTrans.SMS_ID;
                incident.OSC = DepotTrans.OSC;
                BusTotal += Int32.Parse(incident.CUST);
                switch (incident.REGION)
                {
                    case "Northern":
                        NTotal += Int32.Parse(incident.CUST);
                        break;
                    case "Southern":
                        STotal += Int32.Parse(incident.CUST);
                        break;
                    case "North Coast":
                        NcTotal += Int32.Parse(incident.CUST);
                        break;

                }

                if (incident.OSC == "PMQ") { NOscTotal += Int32.Parse(incident.CUST); } else { SOscTotal += Int32.Parse(incident.CUST); }


            }

            var existingReports = _context.EmailManager.Where(i => (i.RestoreStatus == "" || i.RestoreStatus == null) && i.IsDelayed == true);
            int reports = existingReports.Count();
            //string tmp = CommonFunctions.ProcessWhispir("rick.costmeyer@essentialenergy.com.au", "SYS Incident Notification : test", ProcessEmails(noms), "false").GetAwaiter().GetResult();
            if (reports != 0)
            {
                if (BusTotal < 5000)
                {
                    foreach (var item in existingReports)
                    {
                        item.AdditionalMessage = "Significant Customers off supply has returned below 5000 at";
                        item.RestoreStatus = CommonFunctions.ProcessWhispir(Mobiles, whispirTitle + item.AdditionalMessage, "", "false").GetAwaiter().GetResult();
                        
                    }

                    _context.EmailManager.UpdateRange(existingReports);
                    _context.SaveChanges();

                }
                else
                {
                    foreach (var item in existingReports)
                    {
                        DateTime _dateupdated = item.DateUpdated;

                        long diff = _Now.Ticks - _dateupdated.Ticks;
                        TimeSpan elapsedSpan = new TimeSpan(diff);
                        if(elapsedSpan.Minutes >= 30)
                        {
                            item.DateUpdated = _Now;
                            item.ContactList = EmailAddresses(noms);

                            string tmp = CommonFunctions.ProcessWhispir(EmailAddresses(noms), "SYS Incident Notification Api : Significant Customers off Supply Update ", IncidentEmail(spocs, 250, " \n\n"), "false").GetAwaiter().GetResult();
                            
                            _context.EmailManager.Update(item);
                            _context.SaveChanges();
                        }
                        

                    }
                    
                }
            }
            else
            {
                if (BusTotal >= 5000)
                {

                    EmailManager item = new EmailManager();
                    item.TotalCustomers = BusTotal.ToString();
                    item.TotalNorthernCustomers = NTotal.ToString();
                    item.TotalSouthernCustomers = STotal.ToString();
                    item.TotalNorthCoastCustomers = NcTotal.ToString();
                    item.TotalNorthernControlCustomers = NOscTotal.ToString();
                    item.TotalSouthernControlCustomers = SOscTotal.ToString();
                    item.ContactList = Mobiles;
                    item.DateUpdated = _Now;
                    item.IsDelayed = true;
                    item.AdditionalMessage = "Significant Customers off supply has exceeded 5000";
                    item.SentStatus = CommonFunctions.ProcessWhispir(Mobiles, whispirTitle + item.AdditionalMessage, SignificantDetails(item), "false").GetAwaiter().GetResult();
                    string emailMsg = CommonFunctions.ProcessWhispir(EmailAddresses(noms), whispirTitle + item.AdditionalMessage, IncidentEmail(spocs, 250, " \n\n"), "false").GetAwaiter().GetResult();
                    _context.EmailManager.Add(item);
                    _context.SaveChanges();

                }

            }


        }

        void  ProcessRestore(List<Spoc> spocFilter)
        {
            var existingSpocs = _context.Spocs.Where(i => (i.RestoreStatus == "" || i.RestoreStatus == null));

            List<Spoc> tmpSpocs = new List<Spoc>();

            foreach (var incident in existingSpocs)
            {

                if (spocFilter.Any(o => o.INCIDENT_ID == incident.INCIDENT_ID)) { }
                else
                {
                    //find incident in list where it is not a business rule entry and retore it

                    var IncidentRestore = _context.Spocs.SingleOrDefault(b => b.INCIDENT_ID == incident.INCIDENT_ID && !b.BussinessRule);
                    try 
                    {
                        IncidentRestore.CurrentStpis = "";
                        IncidentRestore.AdditionalMessage = "Incident has been Restored on " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString();
                        IncidentRestore.BusTotal = _BusTotal.ToString();
                        switch (incident.REGION)
                        {
                            case "Northern":
                                IncidentRestore.RegionTotal = _NTotal.ToString();
                                break;
                            case "Southern":
                                IncidentRestore.RegionTotal = _STotal.ToString();
                                break;
                            case "North Coast":
                                IncidentRestore.RegionTotal = _NcTotal.ToString();
                                break;

                        }
                        IncidentRestore.RestoreStatus = CommonFunctions.ProcessWhispir(IncidentRestore.ContactList, whispirTitle + IncidentRestore.AdditionalMessage, IncidentDetails(IncidentRestore, " \n"), "false").GetAwaiter().GetResult();
                        IncidentRestore.DateUpdated = _Now;
                        tmpSpocs.Add(IncidentRestore);
                    }
                    catch(Exception ex) { Console.Write(ex.Message); }
                    
                }
            }

            if(tmpSpocs.Count != 0)
            {
                _context.Spocs.UpdateRange(tmpSpocs);
                _context.SaveChanges();
            }
            

        }

        void ProcessRestoreStpis(List<Spoc> spocFilter, List<Nom> noms)
        {
            var existingStpis = _context.Spocs.Where(i => (i.RestoreStatus == "" || i.RestoreStatus == null) && i.BussinessRule && i.AdditionalMessage.Contains("STPIS"));

            List<Spoc> tmpSpocs = new List<Spoc>();
            List<Spoc> emails = new List<Spoc>();
            

            foreach (var incident in existingStpis)
            {
                if (spocFilter.Any(o => o.INCIDENT_ID == incident.INCIDENT_ID))
                {
                    var currentSpoc = spocFilter.SingleOrDefault(o => o.INCIDENT_ID == incident.INCIDENT_ID && o.BussinessRule && o.AdditionalMessage.Contains("STPIS"));
                    var IncidentUpdate = _context.Spocs.SingleOrDefault(b => b.INCIDENT_ID == incident.INCIDENT_ID && b.BussinessRule && b.AdditionalMessage.Contains("STPIS"));
                    DateTime _dateupdated = IncidentUpdate.DateUpdated;

                    long diff = _Now.Ticks - _dateupdated.Ticks;
                    TimeSpan elapsedSpan = new TimeSpan(diff);
                    if (elapsedSpan.Minutes >= 30)
                    {
                        if(Int32.Parse(currentSpoc.CUST) >= 1210)
                        {
                            IncidentUpdate.DateUpdated = _Now;
                            IncidentUpdate.AdditionalMessage = "An incident with STPIS over $50000/ph update at " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString();
                            IncidentUpdate.ContactList = EmailAddressesManagers(noms);
                            emails.Add(IncidentUpdate);
                            tmpSpocs.Add(IncidentUpdate);
                            
                        }
                        else
                        {
                            IncidentUpdate.RestoreStatus = "Restored";
                            IncidentUpdate.DateUpdated = _Now;
                            tmpSpocs.Add(IncidentUpdate);
                        }
                        

                        //_context.EmailManager.Update(item);
                        //_context.SaveChanges();
                    }

                }
                else
                {
                    var IncidentRestore = _context.Spocs.SingleOrDefault(b => b.INCIDENT_ID == incident.INCIDENT_ID && b.BussinessRule && b.AdditionalMessage.Contains("STPIS"));
                    IncidentRestore.RestoreStatus = "Restored";
                    IncidentRestore.DateUpdated = _Now;
                    tmpSpocs.Add(IncidentRestore);


                }
            }
            if (tmpSpocs.Count != 0)
            {
                _context.Spocs.UpdateRange(tmpSpocs);
                _context.SaveChanges();
            }
            if (emails.Count != 0)
            {
                string tmp = CommonFunctions.ProcessWhispir(EmailAddressesManagers(noms), "SYS Incident Notification Api : STPIS report over $50000/ph update at " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString(), IncidentEmail(emails, 250, " \n\n"), "false").GetAwaiter().GetResult();
            }
        }

            void ProcessStpisRules(IEnumerable<Spoc> spocFilter, List<Nom> noms)
        {
            List<Spoc> tmpSpocs = new List<Spoc>();
            

            //1210 ($50000) customers for greater than 10 mins update every 30 mins

            var lst1210 = spocFilter.Where(i => (Int32.Parse(i.CUST) >= 1210 && TimeOff(i.FIRST_SWITCHING_TIME, 10)));
            foreach (var incident in lst1210)
            {
                if (!_context.Spocs.Any(o => (o.INCIDENT_ID == incident.INCIDENT_ID && o.BussinessRule && o.AdditionalMessage.Contains("STPIS"))))
                { 
                    incident.AdditionalMessage = "An incident with STPIS over $50000/ph has exceeded 10 Mins at " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString();
                    incident.BussinessRule = true;
                    incident.DateUpdated = _Now;
                    incident.ContactList = MobileManagers(noms);
                    incident.SentStatus = CommonFunctions.ProcessWhispir(MobileManagers(noms), "System Ops Api : " + incident.AdditionalMessage, IncidentDetails(incident, " \n"), "false").GetAwaiter().GetResult();
                    tmpSpocs.Add(incident);
                }
            }
            if (tmpSpocs.Count != 0)
            {
                _context.Spocs.UpdateRange(tmpSpocs);
                _context.SaveChanges();
            }
           
        }

        void ProcessIpart(List<Nom> noms)
        {
            List<Ipart> tmpIparts = new List<Ipart>();
            string pattern = @"\d{10}";
            var Contacts = noms.Where(b => b.Name == "RecipientsTestMobile");
            string Mobiles = string.Empty;
            foreach (var nom in Contacts)
            {
                string input = nom.Contacts.ToString();

                foreach (Match match in Regex.Matches(input, pattern))
                    Mobiles += match.Value + ";";
            }
            string Emails = string.Empty;
            foreach (var nom in Contacts)
            {
                Emails = nom.Contacts.ToString();

            }

            string url = "http://webviewhomp1/webview/custom/IPART_SP.pl";
            DataSet s = new DataSet();
            string ItemString = string.Empty;
            s = new DataSet();
            s.ReadXml(url);

            //convert table into a json string (equivalent to JSON.stringify)
            if (s.Tables.Count != 0)
            {
                ItemString = JsonConvert.SerializeObject(s.Tables[0]);
                List<Ipart> iparts = JsonConvert.DeserializeObject<List<Ipart>>(ItemString);

                foreach (var incident in iparts)
                {
                    if (!_context.Iparts.Any(o => (o.INCD_REF == incident.INCD_REF)))
                    {
                        var DepotTrans = _context.DepotTranslations.SingleOrDefault(b => b.OZ_NAME == incident.OZ_NAME);
                        incident.OSC = DepotTrans.OSC;
                        //incident.AdditionalMessage = "An incident with STPIS over $50000/ph has exceeded 10 Mins at " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString();
                        //incident.BussinessRule = true;
                        //incident.DateUpdated = _Now;
                        incident.ContactList = ContactsNoms(noms, "IPART Reportable Incidents Group");
                        incident.SentStatus = CommonFunctions.ProcessWhispir(ContactsNoms(noms, "IPART Reportable Incidents Group"), "IPART Reportable Incident sent at " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString(), IpartDetails(incident," \n\n"), "false").GetAwaiter().GetResult();
                        tmpIparts.Add(incident);
                        incident.Voice = true;
                        incident.ContactList = "0428270407";
                        incident.SentStatus = CommonFunctions.ProcessWhispir("0428270407", "IPART Reportable Incident sent at " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString(), IpartDetails(incident, " \n"), "true").GetAwaiter().GetResult();
                        tmpIparts.Add(incident);
                    }
                }
                if (tmpIparts.Count != 0)
                {
                    _context.Iparts.UpdateRange(tmpIparts);
                    _context.SaveChanges();
                }
            }
        }

        void ProcessBusinessRules(IEnumerable<Spoc> spocFilter,List<Nom> noms)
            {
            string pattern = @"\d{10}";
            // var Contacts = noms.Where(b =>  b.Name == "RecipientsInstant");
            var Contacts = noms.Where(b => b.Name == "RecipientsTestMobile");
            string Mobiles = string.Empty;
            foreach (var nom in Contacts)
            {
                string input = nom.Contacts.ToString();

                foreach (Match match in Regex.Matches(input, pattern))
                    Mobiles += match.Value + ";";
            }
            
            

            List<Spoc> tmpSpocs = new List<Spoc>();

            //250 customers for greater than 24 hours

            var lst25024 = spocFilter.Where(i => (Int32.Parse(i.CUST) >= 250  && TimeOff(i.FIRST_SWITCHING_TIME, (24*60))));
            foreach (var incident in lst25024)
            {
                if (!_context.Spocs.Any(o => (o.INCIDENT_ID == incident.INCIDENT_ID && o.BussinessRule)))
                {
                    incident.AdditionalMessage = "An incident with over 250 Customers off has exceeded 24 Hrs at " + _Now.ToLongDateString() + " " + _Now.ToShortTimeString();
                    incident.BussinessRule = true;
                    incident.ContactList = Mobiles;
                    incident.SentStatus = CommonFunctions.ProcessWhispir(Mobiles, "System Ops Api :  " + incident.AdditionalMessage, IncidentDetails(incident, " \n"), "false").GetAwaiter().GetResult();
                    tmpSpocs.Add(incident);
                }
            }

            //100 customers for greater than 11 hours

            var lst10011 = spocFilter.Where(i => (Int32.Parse(i.CUST) >= 100 && TimeOff(i.FIRST_SWITCHING_TIME, 11*60)));
            foreach (var incident in lst10011)
            {
                if (!_context.Spocs.Any(o => (o.INCIDENT_ID == incident.INCIDENT_ID && o.BussinessRule)))
                {
                    incident.AdditionalMessage = "Escalation required Duration now at 11 Hrs at" + _Now.ToLongDateString() + " " + _Now.ToShortTimeString();
                    incident.BussinessRule = true;
                    incident.ContactList = Mobiles;
                    incident.SentStatus = CommonFunctions.ProcessWhispir(Mobiles, "System Ops Api :  " + incident.AdditionalMessage, IncidentDetails(incident, " \n"), "false").GetAwaiter().GetResult();
                    tmpSpocs.Add(incident);
                }
            }

            //2500 customers for greater than 4 hours

            var lst25004 = spocFilter.Where(i => (Int32.Parse(i.CUST) >= 2500 && TimeOff(i.FIRST_SWITCHING_TIME, 4*60)));
            foreach (var incident in lst25004)
            {
                if (!_context.Spocs.Any(o => (o.INCIDENT_ID == incident.INCIDENT_ID && o.BussinessRule)))
                {
                    incident.AdditionalMessage = "An incident with over 2500 Customers off has exceeded 4 Hrs at" + _Now.ToLongDateString() + " " + _Now.ToShortTimeString();
                    incident.BussinessRule = true;
                    incident.ContactList = Mobiles;
                    incident.SentStatus = CommonFunctions.ProcessWhispir(Mobiles, "System Ops Api : " + incident.AdditionalMessage, IncidentDetails(incident, " \n"), "false").GetAwaiter().GetResult();
                    tmpSpocs.Add(incident);
                }
            }

            if (tmpSpocs.Count != 0)
            {
                _context.Spocs.UpdateRange(tmpSpocs);
                _context.SaveChanges();
            }


        }

        public Boolean TimeOff(string startTime,int minutes)
        {
            Boolean result = false;

            if(startTime.IndexOf(".")!=-1)
            { startTime = startTime.Substring(0, startTime.IndexOf(".")); }
            
            DateTime convertedDate = DateTime.Parse(startTime);
            long diff = _Now.Ticks - convertedDate.Ticks;
            TimeSpan elapsedSpan = new TimeSpan(diff);
            if (elapsedSpan.TotalMinutes >= minutes) { result = true; }
            return result;
        }

        public string Duration(string startTime)
        {
            string result = string.Empty;

            if (startTime.IndexOf(".") != -1)
            { startTime = startTime.Substring(0, startTime.IndexOf(".")); }

            DateTime convertedDate = DateTime.Parse(startTime);
            long diff = _Now.Ticks - convertedDate.Ticks;
            TimeSpan elapsedSpan = new TimeSpan(diff);
            string Days = "";
            if(elapsedSpan.TotalDays >= 1)
            {
                Days = elapsedSpan.TotalDays.ToString().Substring(0, elapsedSpan.TotalDays.ToString().IndexOf(".")) + " Days ";
            }

            result =  Days + elapsedSpan.Hours + " Hours " + elapsedSpan.Minutes + " Minutes";
            return result;
        }

        

        public string Stpis(string txt,int minutes)
        {
            double Stpis = ((0.7622 + 0.7342 + 0.6221) / 3);
            double val = Stpis * Int32.Parse(txt) * minutes;
            return string.Format(new System.Globalization.CultureInfo("en-AU"), "{0:C}", val);

           
        }

        public string SignificantDetails(EmailManager obj)
        {
            string datalst = string.Empty;



            
            datalst += "Total EE Customers OFF : " + obj.TotalCustomers + " \n\n";
            datalst += "Total Southern Control Customers : " + obj.TotalSouthernControlCustomers + " \n\n";
            datalst += "Total Northern Control Customers : " + obj.TotalNorthernControlCustomers + " \n\n";
            datalst += "Total Northern Regional Customers : " + obj.TotalNorthernCustomers+ " \n\n";
            datalst += "Total North Coast Regional Customers : " + obj.TotalNorthCoastCustomers + "\n\n";
            datalst += "Total Southern Regional Customers : " + obj.TotalSouthernCustomers + " \n\n";
           

            return datalst;
        }
        public string IpartDetails(Ipart IpartObj, string lineSeperator)
        {
            string datalst = string.Empty;
            datalst += "INCIDENT REF : " + IpartObj.INCD_REF + lineSeperator;
            datalst += "INCD CREATED : " + CommonFunctions.DateFormat(IpartObj.INCD_CREATED) + lineSeperator;
            datalst += "LOCATION : " + IpartObj.LOCATION + lineSeperator;
            datalst += "OPERATION ZONE : " + IpartObj.OZ_NAME + lineSeperator;
            datalst += "OSC : " + IpartObj.OSC + lineSeperator;
            datalst += "STAFF ASSIGNED : " + IpartObj.STAFF_ASSIGNED + lineSeperator;
            datalst += "CRITICAL INFO : " + IpartObj.CRITICAL_INFO + lineSeperator;
            datalst += "REMARKS : " + IpartObj.REMARKS + lineSeperator;
            return datalst;
        }

        public string IncidentDetails(Spoc obj,string lineSeperator)
        {
            string datalst = string.Empty;



           
            datalst += "INCIDENT REF : " + obj.INCIDENT_REF + lineSeperator;
            datalst += "CUST OFF : " + obj.CUST + lineSeperator;
            datalst += "REGION : " + obj.REGION + lineSeperator;

            datalst += "OPERATING ZONE : " + obj.OPERATING_ZONE + lineSeperator;
            datalst += "FEEDER/s : " + obj.FEEDER + "\n\n";
            datalst += "TOTAL CUST. on Feeder/s : " + obj.MaxCustomers + lineSeperator;
            datalst += "DESCRIPTION : " + obj.DESCRIPTION + lineSeperator;
            if (obj.FAULT != "")
			{
                datalst += "FAULT : " + obj.FAULT + lineSeperator;
            }
            else
            {
                datalst += "FAULT : Unknown \n\n";
            }


            if (obj.FAULT_COMMENT != "")
			{
                datalst += "FAULT COMMENT : " + obj.FAULT_COMMENT + lineSeperator;
            }

            datalst += "ZONE SUB/s : " + obj.ZONE_SUB + lineSeperator;
            datalst += "DURATION : " + Duration(obj.FIRST_SWITCHING_TIME) + lineSeperator;
            if (obj.FIRST_SWITCHING_TIME != "")
            {
                datalst += "TIME OFF : " + CommonFunctions.DateFormat(obj.FIRST_SWITCHING_TIME) + lineSeperator;
            }
            else
            {
               datalst += "TIME OFF : " + CommonFunctions.DateFormat(obj.CREATION_DATE) + lineSeperator;
            }

            datalst += "RESOURCE NAME : " + obj.RES_NAME + lineSeperator;
           
            if (obj.EST_REST_DATE != "" )
            {
                datalst += "ETR : " + CommonFunctions.DateFormat(obj.EST_REST_DATE) + lineSeperator;

            }
            else
            {
                datalst += "ETR : Unknown \n\n";
            }
            datalst += "STPIS Per/Hour : " + obj.CurrentStpis + lineSeperator;
            datalst += "TOTAL REG. IMPACT : " + obj.RegionTotal + lineSeperator;
            datalst += "TOTAL BUS. IMPACT : " + obj.BusTotal + lineSeperator;
            //datalst += "SMS ID : " + obj.SMS_ID);

            return datalst;
        }

        public string IncidentEmail(IEnumerable<Spoc> spocFilter, int Custnumbers, string lineSeperator)
        {
            string datalst = string.Empty;

            var spocCutOff = spocFilter.Where(i => Int32.Parse(i.CUST) >= Custnumbers);



            foreach (var obj in spocCutOff)
            {

                
                datalst += IncidentDetails(obj,lineSeperator);
                datalst += "--------------------------------------------------------------------------------\n\n";

            }

            return datalst;
        }


        [HttpGet]
        public IActionResult GetAutomationItems()
        {
            
            
            
            string url = "http://webviewhomp1/webview/custom/ZoneFeederAll.pl";
            DataSet s = new DataSet();
            string ItemString = string.Empty;
            s = new DataSet();
            try
            {
                s.ReadXml(url);
            }
            catch (Exception ex) { Console.Write(ex.Message); }
            //convert table into a json string (equivalent to JSON.stringify)
             if (s.Tables.Count != 0)
             {
            
            
                ItemString = JsonConvert.SerializeObject(s.Tables[0]);

                List<Feeder> feeders = JsonConvert.DeserializeObject<List<Feeder>>(ItemString);
                //return Ok(feeders);
           // }

            string contentRootPath = _hostingEnvironment.ContentRootPath;
            ItemString = System.IO.File.ReadAllText(contentRootPath + "/Noms.json");
            
           //if (ItemString != "")
           // {
                List<Nom> noms = JsonConvert.DeserializeObject<List<Nom>>(ItemString);
           // }

            url = "http://webviewhomp1/webview/custom/log_storm_updates/hv_storm_dashboard.pl";
            s = new DataSet();
            try
            {
                s.ReadXml(url);
            }
            catch(Exception ex) { Console.Write(ex.Message); }
            
            ItemString = string.Empty;
                if (s.Tables.Count != 0)
                {
                    ItemString = JsonConvert.SerializeObject(s.Tables[0]);

                    List<Spoc> spocs = JsonConvert.DeserializeObject<List<Spoc>>(ItemString);
                    var spocFilter = spocs.GroupBy(o => new { o.INCIDENT_ID }).Select(o => o.FirstOrDefault()).Where(i => (i.TYPE == "HVN" && i.STATE == "Conf." && TimeOff(i.CREATION_DATE, 10)));



                    List<Spoc> tmpSpocs = new List<Spoc>();
                    foreach (var incident in spocFilter)
                    {
                        var DepotTrans = _context.DepotTranslations.SingleOrDefault(b => b.OZ_NAME == incident.OPERATING_ZONE);
                        incident.REGION = DepotTrans.REGION;
                        incident.SMS_ID = DepotTrans.SMS_ID;
                        incident.OSC = DepotTrans.OSC;
                        _BusTotal += Int32.Parse(incident.CUST);
                        switch (incident.REGION)
                        {
                            case "Northern":
                                _NTotal += Int32.Parse(incident.CUST);
                                break;
                            case "Southern":
                                _STotal += Int32.Parse(incident.CUST);
                                break;
                            case "North Coast":
                                _NcTotal += Int32.Parse(incident.CUST);
                                break;

                        }

                        if (incident.OSC == "PMQ") { _NOscTotal += Int32.Parse(incident.CUST); } else { _SOscTotal += Int32.Parse(incident.CUST); }

                    }

                    foreach (var incident in spocFilter)
                    {
                        string pattern = @"\d{10}";
                        var Contacts = noms.Where(b => (b.Name == incident.SMS_ID || b.Name == "NOMSNOCR"));
                        var zoneSub = Regex.Matches(incident.ZONE_SUB, ",", RegexOptions.IgnoreCase);
                        if (zoneSub.Count != 0)
                        {
                            incident.AdditionalMessage = "Multiple Zones Affected Possible Subtransmission Fault";
                        }
                        else
                        {
                            var feeder = Regex.Matches(incident.FEEDER, ",", RegexOptions.IgnoreCase);
                            if (feeder.Count != 0)
                            {
                                Contacts = Contacts.Concat(noms.Where(b => b.Name == "NOMS Zone Subs"));
                                incident.AdditionalMessage = "Multiple Feeders Affected Possible Zone Fault";
                            }
                        }



                        string Mobiles = string.Empty;
                        foreach (var nom in Contacts)
                        {
                            string input = nom.Contacts.ToString();

                            foreach (Match match in Regex.Matches(input, pattern))
                                Mobiles += match.Value + ";";
                        }
                        Mobiles += defaultContacts;
                        incident.ContactList = Mobiles;

                        int customerCount = 0;
                        foreach (string fdr in incident.FEEDER.Split(","))
                            try
                            {
                                if (fdr != "No Feeder Data")
                                {
                                    customerCount += Int32.Parse(feeders.SingleOrDefault(b => b.FEEDER == fdr).CUSTOMERS);
                                }
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine(e.Message);
                            }


                        incident.MaxCustomers = customerCount.ToString();
                        incident.CurrentStpis = Stpis(incident.CUST, 60);
                        incident.BusTotal = _BusTotal.ToString();
                        switch (incident.REGION)
                        {
                            case "Northern":
                                incident.RegionTotal = _NTotal.ToString();
                                break;
                            case "Southern":
                                incident.RegionTotal = _STotal.ToString();
                                break;
                            case "North Coast":
                                incident.RegionTotal = _NcTotal.ToString();
                                break;

                        }

                        var cb = Regex.Matches(incident.DESCRIPTION, " CB ", RegexOptions.IgnoreCase);
                        if (_context.Spocs.Any(o => o.INCIDENT_ID == incident.INCIDENT_ID))
                        {
                            var IncidentGroup = _context.Spocs.SingleOrDefault(b => b.INCIDENT_ID == incident.INCIDENT_ID && !b.BussinessRule);


                            IncidentGroup.AGE = incident.AGE;
                            IncidentGroup.FAULT = incident.FAULT;
                            IncidentGroup.FAULT_COMMENT = incident.FAULT_COMMENT;
                            IncidentGroup.EST_REST_DATE = incident.EST_REST_DATE;
                            IncidentGroup.RES_NAME = incident.RES_NAME;
                            IncidentGroup.RES_ETA = incident.RES_ETA;
                            IncidentGroup.DateUpdated = _Now;

                            _context.Spocs.Update(IncidentGroup);
                            _context.SaveChanges();

                        }
                        else
                        {

                            if (cb.Count != 0 || Int32.Parse(incident.CUST) >= (customerCount * 0.9) && customerCount != 0)
                            {
                                incident.SentStatus = CommonFunctions.ProcessWhispir(Mobiles, "SystemOpsApi " + incident.AdditionalMessage, IncidentDetails(incident, " \n"), "false").GetAwaiter().GetResult();
                                //incident.SentStatus = ProcessEmail("rick.costmeyer@essentialenergy.com.au", "SystemOps " + incident.AdditionalMessage, IncidentDetails(incident), "ExportEmail").GetAwaiter().GetResult();
                                //incident.SentStatus = ProcessUrl("http://nocr/spoc_portal/myjson.aspx?func=ExportEmail&Emailto=rick.costmeyer@essentialenergy.com.au&EmailSubject=test&EmailBody=test&EmailCC=").GetAwaiter().GetResult();
                                tmpSpocs.Add(incident);
                                _context.Spocs.Add(incident);
                                _context.SaveChanges();
                            }
                        }

                    }

                    ProcessRestore(spocs);
                    ProcessBusinessRules(spocFilter, noms);
                    ProcessFiveThousandPlusDelayed(spocFilter, noms);
                    ProcessStpisRules(spocFilter, noms);
                    ProcessFiveThousandPlus(spocs, noms);
                    ProcessRestoreStpis(spocs, noms);
                    ProcessIpart(noms);


                    return Ok(spocFilter);
                }
             }


            return Ok();
        }



        }
}