﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Xml;
using Newtonsoft.Json;
using System.Data;
using AutoSms.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace AutoSms
{
    public class SpocData
    {
        public static IHostingEnvironment _hostingEnvironment;
        public SpocData(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            
        }
        static void SaveSmsOptOut(string filename, string _content)
        {
           
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string path = contentRootPath + "/" + filename;

            if (System.IO.File.Exists(path))
            {
                // Note that no lock is put on the 
                // file and the possibility exists 
                // that another process could do 
                // something with it between 
                // the calls to Exists and Delete.
                System.IO.File.Delete(path);
            }

            // Create the file. 
            using (FileStream fs = System.IO.File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(_content);
                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }
           
        }

        static async Task<String> ProcessSmsOptOut()
        {
            String stringTask = string.Empty;
            try
            {
                SqlConnection conn = new SqlConnection
                {
                    ConnectionString =
                "Data Source= PCE9ODSMSQLLIVE;" +
                "Initial Catalog=ODSPCE9_AUXDB;" +
                "Integrated Security=True;"
                };

                string sql = "SELECT [PREMNUM],[CONSUMER_NAME],[SMS_OPT_OUT] FROM [REPORTING].[VW_R128_CUSTOMER_DATA_EXTRACT_PART1] WHERE([REPORTING].[VW_R128_CUSTOMER_DATA_EXTRACT_PART1].[SMS_OPT_OUT] = 'Y');";
              
                await conn.OpenAsync();
                
                SqlCommand command = new SqlCommand(sql, conn)
                {

                    CommandTimeout = 3000
                };
                var adapter = new SqlDataAdapter(command);

                // use the connection here
                
                var s = new DataSet();
                adapter.Fill(s);

                conn.Close();
                conn.Dispose();
                stringTask = string.Empty;
                if (s.Tables.Count != 0)
                {
                    stringTask = JsonConvert.SerializeObject(s.Tables[0]);



                }
                

            }
            catch (Exception ex)
            { stringTask = ex.Message; }



            return stringTask;
        }

        static async Task<String> ProcessPof(string _address)
        {


            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Add("Ticket", "77FC7886-45CC-11E7-A61B-4A3281D1878A");
            String stringTask = null;//client.GetStringAsync(_address);
            HttpResponseMessage response = await client.GetAsync(_address);
            if (response.IsSuccessStatusCode)
            {
                stringTask = await response.Content.ReadAsStringAsync();

            }

            return stringTask;

        }
       

        public static void LoadSpocData()
        {
            string mainAddress = "http://localhost:60295/api/Auto";
            try
            {
                var itemString = ProcessPof(mainAddress).GetAwaiter().GetResult();
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        public static void LoadLsData()
        {
            string mainAddress = "http://localhost:60295/api/LS";
            try
            {
                var itemString = ProcessPof(mainAddress).GetAwaiter().GetResult();
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        public static void LoadNar24Data()
        {
            string mainAddress = "http://localhost:60295/api/Nar";
            try
            {
                var itemString = ProcessPof(mainAddress).GetAwaiter().GetResult();
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        public static void LoadNar72Data()
        {
            string mainAddress = "http://localhost:60295/api/Nar72";
            try
            {
                var itemString = ProcessPof(mainAddress).GetAwaiter().GetResult();
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        public static void RebootData()
        {
            string mainAddress = "http://localhost:60295/api/Reboot";
            try
            {
                var itemString = ProcessPof(mainAddress).GetAwaiter().GetResult();
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        public static void PatchData()
        {
            string mainAddress = "http://localhost:60295/api/Patch";
            try
            {
                var itemString = ProcessPof(mainAddress).GetAwaiter().GetResult();
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        public static void CartographerData()
        {
            string mainAddress = "http://localhost:60295/api/Cartographer";
            try
            {
                var itemString = ProcessPof(mainAddress).GetAwaiter().GetResult();
            }
            catch (Exception ex) { Console.Write(ex.Message); }
        }

        public static void SmsOptOutData()
        {
            string data = string.Empty;
            try
            {
                data = ProcessSmsOptOut().GetAwaiter().GetResult();
                if (data.IndexOf("{") != -1)
                {
                    
                    SaveSmsOptOut("SmsOptOut.json", data);
                }
            }
            catch (Exception ex) { Console.Write(ex.Message); }
            
        }



    }
}
