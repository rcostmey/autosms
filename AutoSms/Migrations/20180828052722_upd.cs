﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AutoSms.Migrations
{
    public partial class upd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DepotTranslations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OZ_NAME = table.Column<string>(nullable: true),
                    DISTRICT = table.Column<string>(nullable: true),
                    REGION = table.Column<string>(nullable: true),
                    OSC = table.Column<string>(nullable: true),
                    label = table.Column<string>(nullable: true),
                    SMS_ID = table.Column<string>(nullable: true),
                    Lat = table.Column<string>(nullable: true),
                    Lng = table.Column<string>(nullable: true),
                    Coordinates = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepotTranslations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmailManager",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TotalCustomers = table.Column<string>(nullable: true),
                    TotalNorthernControlCustomers = table.Column<string>(nullable: true),
                    TotalSouthernControlCustomers = table.Column<string>(nullable: true),
                    TotalSouthernCustomers = table.Column<string>(nullable: true),
                    TotalNorthernCustomers = table.Column<string>(nullable: true),
                    TotalNorthCoastCustomers = table.Column<string>(nullable: true),
                    AdditionalMessage = table.Column<string>(nullable: true),
                    SentStatus = table.Column<string>(nullable: true),
                    RestoreStatus = table.Column<string>(nullable: true),
                    ContactList = table.Column<string>(nullable: true),
                    IsDelayed = table.Column<bool>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailManager", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Iparts",
                columns: table => new
                {
                    IpartId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    INCD_REF = table.Column<string>(nullable: true),
                    INCD_CREATED = table.Column<string>(nullable: true),
                    LOCATION = table.Column<string>(nullable: true),
                    OZ_NAME = table.Column<string>(nullable: true),
                    OSC = table.Column<string>(nullable: true),
                    STAFF_ASSIGNED = table.Column<string>(nullable: true),
                    CRITICAL_INFO = table.Column<string>(nullable: true),
                    REMARKS = table.Column<string>(nullable: true),
                    ContactList = table.Column<string>(nullable: true),
                    Voice = table.Column<bool>(nullable: false),
                    SentStatus = table.Column<string>(nullable: true),
                    DateUpdated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iparts", x => x.IpartId);
                });

            migrationBuilder.CreateTable(
                name: "Spocs",
                columns: table => new
                {
                    SpocId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    INCIDENT_ID = table.Column<string>(nullable: true),
                    ACCESS_ISSUE = table.Column<string>(nullable: true),
                    AGE = table.Column<string>(nullable: true),
                    ASSN = table.Column<string>(nullable: true),
                    CANC = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<string>(nullable: true),
                    CUST = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    DISP = table.Column<string>(nullable: true),
                    EST_REST_DATE = table.Column<string>(nullable: true),
                    FAULT = table.Column<string>(nullable: true),
                    FAULT_COMMENT = table.Column<string>(nullable: true),
                    FEEDER = table.Column<string>(nullable: true),
                    FIRST_SWITCHING_TIME = table.Column<string>(nullable: true),
                    HRS_SINCE_SUPP_UPDATE = table.Column<string>(nullable: true),
                    INCIDENT_REF = table.Column<string>(nullable: true),
                    INC_CATEGORY_NAME = table.Column<string>(nullable: true),
                    JOB_ID = table.Column<string>(nullable: true),
                    LAST_UPDATE = table.Column<string>(nullable: true),
                    LS_COUNT = table.Column<string>(nullable: true),
                    NUM_OF_CALLS = table.Column<string>(nullable: true),
                    OPERATING_ZONE = table.Column<string>(nullable: true),
                    OUTSTANDING_CRIT_CALLS = table.Column<string>(nullable: true),
                    OVERNIGHT = table.Column<string>(nullable: true),
                    READY_TO_COMPLETE = table.Column<string>(nullable: true),
                    REGION = table.Column<string>(nullable: true),
                    RES_ETA = table.Column<string>(nullable: true),
                    RES_NAME = table.Column<string>(nullable: true),
                    RES_STATUS = table.Column<string>(nullable: true),
                    SECONDARY_ALIAS = table.Column<string>(nullable: true),
                    STATE = table.Column<string>(nullable: true),
                    SUPP_MESSAGE = table.Column<string>(nullable: true),
                    SWITCHING = table.Column<string>(nullable: true),
                    TYPE = table.Column<string>(nullable: true),
                    WAS_PRINTED = table.Column<string>(nullable: true),
                    ZONE_SUB = table.Column<string>(nullable: true),
                    OSC = table.Column<string>(nullable: true),
                    SMS_ID = table.Column<string>(nullable: true),
                    MaxCustomers = table.Column<string>(nullable: true),
                    AdditionalMessage = table.Column<string>(nullable: true),
                    SentStatus = table.Column<string>(nullable: true),
                    RestoreStatus = table.Column<string>(nullable: true),
                    CurrentStpis = table.Column<string>(nullable: true),
                    ContactList = table.Column<string>(nullable: true),
                    RegionTotal = table.Column<string>(nullable: true),
                    BusTotal = table.Column<string>(nullable: true),
                    BussinessRule = table.Column<bool>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spocs", x => x.SpocId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DepotTranslations");

            migrationBuilder.DropTable(
                name: "EmailManager");

            migrationBuilder.DropTable(
                name: "Iparts");

            migrationBuilder.DropTable(
                name: "Spocs");
        }
    }
}
